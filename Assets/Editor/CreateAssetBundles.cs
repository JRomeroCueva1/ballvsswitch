﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace Editor
{
    internal class AssetBundleExportWindow : EditorWindow
    {
        private int versionNumber = 1;
        private string exportDirectory = "Assets/AssetBundles";

        private readonly HashSet<BuildTarget> selectedPlatforms = new HashSet<BuildTarget>
        {
            BuildTarget.Android,
            BuildTarget.iOS,
            BuildTarget.StandaloneLinux,
            BuildTarget.StandaloneWindows
        };

        private readonly List<BuildTarget> supportedPlatforms = new List<BuildTarget>
        {
            BuildTarget.Android,
            BuildTarget.iOS,
            BuildTarget.StandaloneLinux,
            BuildTarget.StandaloneOSX,
            BuildTarget.StandaloneWindows
        };

        private void OnGUI()
        {
            GUILayout.Label("Target Directory", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Label(exportDirectory, GUILayout.Width(100));
            if (GUILayout.Button("...", GUILayout.Width(25)))
                UpdateExportDirectory();
            GUILayout.EndHorizontal();

            GUILayout.Label("Export Platforms", EditorStyles.boldLabel);
            foreach (BuildTarget target in supportedPlatforms)
            {
                GUILayout.BeginHorizontal();
                UpdateSelectedPlatformItem(
                    EditorGUILayout.Toggle(selectedPlatforms.Contains(target), GUILayout.Width(10)),
                    target);
                GUILayout.Label(target.ToString().ToLower(), GUILayout.Width(100));
                GUILayout.EndHorizontal();
            }

            GUILayout.Label("Version Number", EditorStyles.boldLabel);
            GUILayout.BeginHorizontal();
            GUILayout.Label("Number:", GUILayout.Width(100));
            versionNumber = EditorGUILayout.IntField(versionNumber, GUILayout.Width(25));
            GUILayout.EndHorizontal();

            if (GUILayout.Button("BUILD", EditorStyles.miniButtonMid, GUILayout.Height(50)))
            {
                Debug.ClearDeveloperConsole();
                foreach (BuildTarget platform in selectedPlatforms)
                    CreateAssetBundles.Build(platform, exportDirectory, versionNumber);
            }
        }

        private void UpdateSelectedPlatformItem(bool toggle, BuildTarget target)
        {
            if (toggle) selectedPlatforms.Add(target);
            else selectedPlatforms.Remove(target);
        }

        private void UpdateExportDirectory()
        {
            string chosenDirectory = EditorUtility.OpenFolderPanel("AssetBundle Save Folder", exportDirectory, "");
            if (string.IsNullOrEmpty(chosenDirectory)) return;
            exportDirectory = chosenDirectory;
        }
    }

    internal static class CreateAssetBundles
    {
        [MenuItem("Assets/Build AssetBundles")]
        private static void OpenWindow()
        {
            EditorWindow.GetWindow<AssetBundleExportWindow>();
        }

        internal static void Build(BuildTarget platform, string saveDirectory, int versionNumber)
        {
            MkDir(saveDirectory);

            string platformName = GetPlatformName(platform);
            string tmpDir = string.Format("{0}/{1}", saveDirectory, platformName);

            MkDir(tmpDir);
            BuildPipeline.BuildAssetBundles(tmpDir, BuildAssetBundleOptions.None, platform);
            CleanUpMetaManifest(platformName, tmpDir);
            foreach (string file in Directory.GetFiles(tmpDir))
                SafeMove(
                    file,
                    string.Format("{0}/{1}{2}{3}", saveDirectory, Path.GetFileName(file), platformName, versionNumber));

            Directory.Delete(tmpDir);
        }

        private static void CleanUpMetaManifest(string platformName, string outputPath)
        {
            DeleteIfExists(string.Format("{0}/{1}", outputPath, platformName));
            DeleteIfExists(string.Format("{0}/{1}.manifest", outputPath, platformName));

            foreach (string file in new DirectoryInfo(outputPath).GetFiles("*.manifest").Select(f => f.FullName))
                File.Delete(file);

            foreach (string file in new DirectoryInfo(outputPath).GetFiles("*.meta").Select(f => f.FullName))
                File.Delete(file);
        }

        private static string GetPlatformName(BuildTarget buildTarget)
        {
            switch (buildTarget)
            {
                case BuildTarget.Android:
                    return "android";

                case BuildTarget.iOS:
                    return "ios";

                case BuildTarget.StandaloneLinux:
                    return "linux";

                default:
                    return "standalonewindows";
            }
        }

        private static void MkDir(string saveDirectory)
        {
            if (!Directory.Exists(saveDirectory)) Directory.CreateDirectory(saveDirectory);
        }

        private static void SafeMove(string srcPath, string dstPath)
        {
            Assert.IsTrue(File.Exists(srcPath), "Src file does not exist !");
            if (File.Exists(dstPath)) File.Delete(dstPath);
            File.Move(srcPath, dstPath);
        }

        private static void DeleteIfExists(string path)
        {
            if (File.Exists(path)) File.Delete(path);
        }
    }
}
