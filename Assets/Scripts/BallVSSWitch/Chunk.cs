﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class Chunk : MonoBehaviour
    {
        public Image graphic;
        public Image instructionGraphic;
        public Text valueText;
        public Animator chunkAnimator;
        public chunkAnimationStyle animationStyle;
        public string chunkCode;
        public chunkColor chColor;
        public RectTransform rectTrans;
        public bool is_open;
        public List<RectTransform> waypoints;
        public int value;

        public enum chunkAnimationStyle
        {
            None,
            Rotate,
            LeftRotate,
            RightRotate,
            TranslateRightToLeft,
            Bump,
            Extend
        }

        public enum chunkColor
        {
            White,
            Red,
            Blue
        }

        public void PlayAnimation(chunkAnimationStyle animation_style)
        {
            chunkAnimator.Play(animation_style.ToString(), 0, 0);
        }

        public void ForcePlayOpen()
        {
            chunkAnimator.Play("Open", 0, 0);
        }

        public void Open()
        {
            chunkAnimator.SetTrigger("Open");
            is_open = true;
        }

        public void Close()
        {
            chunkAnimator.ResetTrigger("Open");
            chunkAnimator.Play(animationStyle.ToString(), 0, 0);
            is_open = false;
        }

        public void BreakBlock()
        {
            if (!is_open)
            {
                chunkAnimator.ResetTrigger("Open");
                chunkAnimator.Play("Break", 0, 0);
                FXManager.Instance.ApplyFx(valueText.transform.position, FXManager.fxName.WallExplode, 2);
                is_open = true;
            }
        }

        public void Toggle()
        {
            if (animationStyle != chunkAnimationStyle.Bump)
            {
                if (is_open)
                    Close();
                else
                    Open();
            }
            else
            {
                if (value <= 1)
                    BreakBlock();
                else
                {
                    PlayAnimation(animationStyle);
                    FXManager.Instance.ApplyFx(valueText.transform.position, FXManager.fxName.BallExplode, 2);
                }
            }
            if (rectTrans.anchoredPosition.y < 1100)
            {
                value--;
                SetValueText();
            }
        }

        public void SetTextPosition()
        {
            if (chunkCode != null && chunkCode.Length > 3)
            {
                if (chunkCode[1] == '7')
                {
                    valueText.rectTransform.offsetMin = new Vector2(-100, 0);
                }
                if (chunkCode[1] == '8')
                {
                    valueText.rectTransform.offsetMin = new Vector2(0, 0);
                }
                if (chunkCode[1] == '9')
                {
                    valueText.rectTransform.offsetMin = new Vector2(100, 0);
                }
            }
        }

        public void SetValueText()
        {
            if (value > 0)
                valueText.text = value.ToString();
            else
                valueText.text = "";
        }
    }
}