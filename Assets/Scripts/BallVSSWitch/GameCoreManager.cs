﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class GameCoreManager : MonoBehaviour
    {
        private static GameCoreManager instance = null;
        public RunManager run_manager;
        public LeaderBoardManager leaderboard_manager;
        public InputManager input_manager;

        public static GameCoreManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<GameCoreManager>();
                }
                return instance;
            }
            set { }
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        void Start()
        {
            Application.targetFrameRate = 60;
            StartGame(true);
        }

        public void NewGame()
        {
            if (run_manager.session_status == RunManager.SessionStatus.None)
            {
                ScoreManager.Instance.SetupScores();
                FXManager.Instance.ResetBackgroundVariant();
                run_manager.Reinit();
                input_manager.Reinit();
                newGamePanel.gameObject.SetActive(false);
                retryGamePanel.gameObject.SetActive(false);
            }
        }

        public RectTransform newGamePanel;
        public RectTransform retryGamePanel;
        public void StartGame(bool first)
        {
            ScoreManager.Instance.SetStartMenuScores();
            leaderboard_manager.SetLeaderboard();
            run_manager.ChangeSessionStatus(RunManager.SessionStatus.None);
            if (first)
                newGamePanel.gameObject.SetActive(true);
            else
                retryGamePanel.gameObject.SetActive(true);
            run_manager.CleanAllChunks();
        }

        public void NewGame(UnityEngine.EventSystems.BaseEventData be)
        {
            NewGame();
        }
    }
}