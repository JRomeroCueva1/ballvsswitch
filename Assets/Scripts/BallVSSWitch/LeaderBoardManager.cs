﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class LeaderBoardManager : MonoBehaviour
    {

        public Text highscoreText;
        public Text rankText;

        public Text aboveHighscoreText;
        public Text aboveRankText;

        public Text belowHighscoreText;
        public Text belowRankText;

        public void SetLeaderboard()
        {
            int highscore = ScoreManager.Instance.current_highscore;
            int rank = GetRankForScore(highscore);
            int below_score = (highscore - Random.Range(1, 10));
            if (below_score < 0)
                below_score = 0;
            if (highscore >= worldRecord)
            {
                rank = 1;
                aboveHighscoreText.transform.parent.gameObject.SetActive(false);
            }


            highscoreText.text = highscore.ToString();
            rankText.text = "#" + rank;
            aboveHighscoreText.text = (highscore + Random.Range(1, 10)).ToString();
            belowHighscoreText.text = below_score.ToString();
            aboveRankText.text = "#" + (rank - 1);
            belowRankText.text = "#" + (rank + 1);
        }


        public float worldRecord = 2500;
        public float participants = 12000;

        public int GetRankForScore(int score)
        {
            float score_scale = (1 - 0) / (worldRecord - 0) * (score - worldRecord) + 1;
            float denormalized_rank = score_scale * (participants - 1) + 1;
            float my_rank = participants - denormalized_rank;
            return (int)my_rank;
        }
    }
}