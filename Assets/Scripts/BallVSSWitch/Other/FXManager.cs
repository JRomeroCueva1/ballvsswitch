﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Rendering;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class FXManager : MonoBehaviour
    {
        private List<GameObject> FxPool;
        public Transform fxPoolHolder;
        public GameObject fxPrefab;
        public Dictionary<fxName, string> fx_list;
        public Animator backgroundAnimator;
        public Image backgroundImage;
        public Sprite[] backgroundVariants;

        public UpFXManager upFXPool;
        public Transform missFXAnchor;

        public bool initialized = false;

        public enum fxName
        {
            None,
            Plomo,
            BallExplode,
            WallExplode
        };

        public enum backGroundAnimations
        {
            None,
            Miss,
            ComboUp
        };

        private static FXManager instance = null;

        public static FXManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<FXManager>();
                }
                return instance;
            }
            set { }
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            InitPool();
        }

        public void InitPool()
        {
            initialized = true;
            if (fxPoolHolder != null && (FxPool == null || FxPool.Count == 0))
            {
                FxPool = new List<GameObject>();
                foreach (Transform proj in fxPoolHolder)
                {
                    FxPool.Add(proj.gameObject);
                }
            }
            fxName n = fxName.None;
            fx_list = new Dictionary<fxName, string>();
            foreach (string fx in System.Enum.GetNames(typeof(fxName)))
            {
                fx_list.Add(n, fx);
                n++;
            }
        }

        GameObject GetPooledFX(fxName selected_fx)
        {
            if (!initialized)
                InitPool();
            if (selected_fx == fxName.None)
                return null;
            foreach (GameObject proj in FxPool)
            {
                if (proj != null && Vector3.Distance(proj.transform.position, fxPoolHolder.position) < 1)
                {
                    ActivateFX(proj, selected_fx, true);
                    return proj;
                }
            }
            GameObject new_proj = (GameObject)Instantiate(fxPrefab, fxPoolHolder.position, fxPoolHolder.rotation);
            new_proj.transform.SetParent(fxPoolHolder.transform);
            new_proj.transform.position = fxPoolHolder.position;
            ActivateFX(new_proj, selected_fx, true);
            FxPool.Add(new_proj);
            return new_proj;
        }

        void ActivateFX(GameObject projectile, fxName selected_fx, bool activate)
        {
            projectile.SetActive(activate);
            string fx_string = "None";
            fx_list.TryGetValue(selected_fx, out fx_string);
            foreach (Transform fx in projectile.transform)
            {
                if (activate)
                {
                    if (fx.name.Equals(fx_string))
                        fx.gameObject.SetActive(activate);
                    else
                        fx.gameObject.SetActive(!activate);
                }
                else
                    fx.gameObject.SetActive(activate);
            }
        }

        public void ReturnPooledFX(GameObject returnedFX)
        {
            if (returnedFX == null)
                return;
            ActivateFX(returnedFX, 0, false);
            returnedFX.transform.SetParent(fxPoolHolder);
            returnedFX.gameObject.transform.position = fxPoolHolder.position;
            returnedFX.transform.localPosition = Vector3.zero;
            returnedFX.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }

        public GameObject ApplyFx(GameObject target, fxName fx, float max_duration)
        {
            if (fx == fxName.None)
                return null;
            GameObject selected_fx = GetPooledFX(fx);

            selected_fx.transform.SetParent(target.transform);
            selected_fx.transform.localPosition = Vector3.zero;
            selected_fx.transform.localScale = Vector3.one;
            if (max_duration > 0)
                StartCoroutine(StopAppliedFx(selected_fx, max_duration));
            return selected_fx;
        }

        public GameObject ApplyFx(GameObject target, Vector3 position, fxName fx, float max_duration)
        {
            if (fx == fxName.None)
                return null;
            GameObject selected_fx = GetPooledFX(fx);

            selected_fx.transform.position = position;
            selected_fx.transform.SetParent(target.transform);

            if (max_duration > 0)
                StartCoroutine(StopAppliedFx(selected_fx, max_duration));
            return selected_fx;
        }

        public GameObject ApplyFx(Vector3 position, fxName fx, float max_duration)
        {
            if (fx == fxName.None)
                return null;
            GameObject selected_fx = GetPooledFX(fx);

            selected_fx.transform.position = position;
            if (max_duration > 0)
                StartCoroutine(StopAppliedFx(selected_fx, max_duration));
            return selected_fx;
        }

        IEnumerator StopAppliedFx(GameObject selected_fx, float max_duration)
        {
            yield return new WaitForSeconds(max_duration);
            ReturnPooledFX(selected_fx);
        }


        public void PlayBackgroundAnimation(backGroundAnimations bg_anim)
        {
            if (bg_anim == backGroundAnimations.Miss)
            {
                backgroundAnimator.SetTrigger("Miss");
            }
            else if (bg_anim == backGroundAnimations.ComboUp)
            {
                backgroundAnimator.SetTrigger("ComboUp");

            }
        }

        int currentBackGroundVariant = 0;
        public void SwitchBackgroundColor()
        {
            if (currentBackGroundVariant >= backgroundVariants.Length)
                currentBackGroundVariant = 0;
            backgroundImage.sprite = backgroundVariants[currentBackGroundVariant];
            currentBackGroundVariant++;
        }

        public void ResetBackgroundVariant()
        {
            currentBackGroundVariant = 0;
        }
    }
}