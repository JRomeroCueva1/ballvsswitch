﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class InputManager : MonoBehaviour
    {
        public RunManager run_manager;
        public Animator redAnimator;
        public Animator blueAnimator;

        public void InteractBlueStuff(BaseEventData data)
        {
            blueAnimator.Play("Press", 0, 0);
            run_manager.ToggleChunks(Chunk.chunkColor.Blue);
        }

        public void InteractRedStuff(BaseEventData data)
        {
            redAnimator.Play("Press", 0, 0);
            run_manager.ToggleChunks(Chunk.chunkColor.Red);
        }

        public void Reinit()
        {
            blueAnimator.Play("Ready", 0, 0);
            redAnimator.Play("Ready", 0, 0);
        }
    }
}