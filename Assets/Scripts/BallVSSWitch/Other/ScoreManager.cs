﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class ScoreManager : MonoBehaviour
    {

        private static ScoreManager instance = null;

        public static ScoreManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ScoreManager>();
                }
                return instance;
            }
            set { }
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
        }

        public Text scoreText;
        public Text highScoreText;
        public Text retryMenuScoreText;
        public Text startMenuHighScoreText;
        public Animator scoreAnimator;

        public int current_score;
        public int current_highscore;

        public void SetupScores()
        {
            InitHighscore();
            ResetScore();
        }

        public void SetStartMenuScores()
        {
            InitHighscore();
            if (current_score > 0)
                retryMenuScoreText.text = current_score.ToString();
            else
                retryMenuScoreText.text = "0";
            startMenuHighScoreText.text = current_highscore.ToString();
        }

        void InitHighscore()
        {
            if (!PlayerPrefs.HasKey("BallVSSWitch:Highscore"))
            {
                PlayerPrefs.SetInt("BallVSSWitch:Highscore", 0);
            }
            current_highscore = PlayerPrefs.GetInt("BallVSSWitch:Highscore");
            SetHighScore(current_highscore);
        }

        void SetScore(int score)
        {
            if (score > 0)
                scoreText.text = score.ToString();
            else
                scoreText.text = "0";
        }

        void SetHighScore(int highscore)
        {
            if (highscore > 0)
                highScoreText.text = "HIGHSCORE : " + highscore;
            else
                highScoreText.text = "HIGHSCORE : " + "0";
            PlayerPrefs.SetInt("BallVSSWitch:Highscore", highscore);
        }

        public void AddScore(int multiplier)
        {
            //scoreAnimator.Play("bump");
            current_score += multiplier;
            SetScore(current_score);
            if (current_score > current_highscore)
                SetHighScore(current_score);
        }

        public void RemoveScore()
        {
            current_score--;
            SetScore(current_score);
        }

        public void ResetScore()
        {
            current_score = 0;
            SetScore(current_score);
        }
    }
}