﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class UpFXManager : MonoBehaviour
    {
        [System.Serializable]
        class UpFX
        {
            public GameObject fxRoot { get; private set; }
            private Animator fxAnimator;
            public bool IsBusy { get; private set; }

            public UpFX(GameObject root, Transform parent, bool is_new)
            {
                this.fxRoot = root;
                if (is_new)
                {
                    this.fxRoot.transform.SetParent(parent);
                }
                this.fxRoot.transform.localScale = new Vector3(1, 1, 1);
                this.fxRoot.transform.position = Vector3.zero;
                this.fxRoot.transform.rotation = Quaternion.identity;
                this.fxRoot.SetActive(false);
                this.fxAnimator = this.fxRoot.GetComponentInChildren<Animator>();
                this.IsBusy = false;
            }

            public void SetPosition(Vector3 world_position)
            {
                this.fxRoot.transform.position = world_position;
            }

            public void Play()
            {
                this.IsBusy = true;
                this.fxRoot.SetActive(true);
                this.fxAnimator.Play("up", 0, 0);
            }

            public void Stop()
            {
                this.IsBusy = false;
                this.fxRoot.SetActive(false);
            }
        }

        public GameObject scoreFXPrefab;
        public GameObject hearthFXPrefab;
        public Transform scoreFXPool;
        public Transform hearthFXPool;

        private const int FX_NBR_LIMIT = 25;
        public float DELAY_TO_STOP = .3f;
        private List<UpFX> score_fxs_list = new List<UpFX>();
        private List<UpFX> hearth_fxs_list = new List<UpFX>();

        private void Awake()
        {
            foreach (Transform child in scoreFXPool)
            {
                CreateFX(child.gameObject, scoreFXPool, score_fxs_list, false);
            }
            foreach (Transform child in hearthFXPool)
            {
                CreateFX(child.gameObject, hearthFXPool, hearth_fxs_list, false);
            }
        }

        public void PlayScoreUpFX(Vector3 position, int score)
        {
            UpFX new_fx = PlayFX(scoreFXPrefab, scoreFXPool, score_fxs_list, position);
            // moche mais quick
            Text score_txt = new_fx.fxRoot.transform.GetComponentInChildren<Text>();
            if (score_txt != null)
                score_txt.text = "+" + score;
        }

        public void PlayMissUpFX(Vector3 position)
        {
            UpFX new_fx = PlayFX(scoreFXPrefab, scoreFXPool, score_fxs_list, position);
            // moche mais quick
            Text score_txt = new_fx.fxRoot.transform.GetComponentInChildren<Text>();
            if (score_txt != null)
                score_txt.text = "MISS";
        }

        public void PlayHearthUpFX(Vector3 position)
        {
            PlayFX(hearthFXPrefab, hearthFXPool, hearth_fxs_list, position);
        }

        private UpFX PlayFX(GameObject prefab, Transform parent, List<UpFX> list, Vector3 position)
        {
            UpFX new_fx = GetFreeFX(prefab, parent, list);
            if (new_fx == null) // FX_NBR_LIMIT limit is reached
                return null;
            new_fx.SetPosition(position);
            new_fx.Play();
            StartCoroutine(StopFX(new_fx, DELAY_TO_STOP));
            return new_fx;
        }

        private IEnumerator StopFX(UpFX fx, float delay_to_stop)
        {
            yield return new WaitForSeconds(delay_to_stop);
            fx.Stop();
        }

        private UpFX GetFreeFX(GameObject prefab, Transform parent, List<UpFX> list)
        {
            for (int i = 0; i < list.Count; ++i)
            {
                if (!list[i].IsBusy)
                    return list[i];
            }
            return GetNewFX(prefab, parent, list);
        }

        private UpFX GetNewFX(GameObject prefab, Transform parent, List<UpFX> list)
        {
            if (list.Count < FX_NBR_LIMIT)
            {
                return CreateFX(GameObject.Instantiate(prefab), parent, list, true);
            }
            return null;
        }

        private UpFX CreateFX(GameObject obj, Transform parent, List<UpFX> list, bool is_new)
        {
            UpFX inst = new UpFX(obj, parent, is_new);
            list.Add(inst);
            return inst;
        }
    }
}