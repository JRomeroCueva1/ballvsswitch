﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class PatternManager : MonoBehaviour
    {

        public ChunkGenerator chunk_generator;

        [System.Serializable]
        public class PatternArrayLayout
        {
            [System.Serializable]
            public struct rowData
            {
                public obstacle[] row;
            }

            public rowData[] rows = new rowData[7]; //Grid of 7x7
        }

        public enum obstacle
        {
            None,
            OPEN_RED,
            CLOSED_RED,
            OPEN_BLUE,
            CLOSED_BLUE,
            WALL_BLUE,
            WALL_RED
        }

        public PatternArrayLayout initial_layout;

        [Header("Easy patterns")]
        public PatternArrayLayout easyLayout1;
        public PatternArrayLayout easyLayout2;
        public PatternArrayLayout easyLayout3;
        public PatternArrayLayout easyLayout4;
        public PatternArrayLayout easyLayout5;
        public PatternArrayLayout easyLayout6;
        public PatternArrayLayout easyLayout7;
        public PatternArrayLayout easyLayout8;
        public PatternArrayLayout easyLayout9;
        public PatternArrayLayout easyLayout10;

        [Header("Medium patterns")]
        public PatternArrayLayout mediumLayout1;
        public PatternArrayLayout mediumLayout2;
        public PatternArrayLayout mediumLayout3;
        public PatternArrayLayout mediumLayout4;
        public PatternArrayLayout mediumLayout5;
        public PatternArrayLayout mediumLayout6;
        public PatternArrayLayout mediumLayout7;
        public PatternArrayLayout mediumLayout8;
        public PatternArrayLayout mediumLayout9;
        public PatternArrayLayout mediumLayout10;

        [Header("Hard patterns")]
        public PatternArrayLayout hardLayout1;
        public PatternArrayLayout hardLayout2;
        public PatternArrayLayout hardLayout3;
        public PatternArrayLayout hardLayout4;
        public PatternArrayLayout hardLayout5;
        public PatternArrayLayout hardLayout6;
        public PatternArrayLayout hardLayout7;
        public PatternArrayLayout hardLayout8;
        public PatternArrayLayout hardLayout9;
        public PatternArrayLayout hardLayout10;

        [Header("Extreme patterns")]
        public PatternArrayLayout extremeLayout1;
        public PatternArrayLayout extremeLayout2;
        public PatternArrayLayout extremeLayout3;
        public PatternArrayLayout extremeLayout4;
        public PatternArrayLayout extremeLayout5;
        public PatternArrayLayout extremeLayout6;
        public PatternArrayLayout extremeLayout7;
        public PatternArrayLayout extremeLayout8;
        public PatternArrayLayout extremeLayout9;
        public PatternArrayLayout extremeLayout10;

        public List<PatternArrayLayout> pattern_layouts;

        public class Pattern
        {
            public List<Chunk> column1;
            public List<Chunk> column2;
        }

        public Pattern GivePattern(Pattern previous_pattern, float position_offset)
        {
            if (pattern_layouts == null)
            {
                pattern_layouts = new List<PatternArrayLayout>();
                AddEasyLayouts();
            }

            Pattern pattern = new Pattern();
            PatternArrayLayout selected_layout = initial_layout;
            if (position_offset != 0)
                selected_layout = GetRandomLayout();
            pattern.column1 = GiveColumn(selected_layout, 1, previous_pattern != null ? previous_pattern.column1 : null, position_offset);
            pattern.column2 = GiveColumn(selected_layout, 2, previous_pattern != null ? previous_pattern.column2 : null, position_offset);

            return pattern;
        }

        public void AddEasyLayouts()
        {
            pattern_layouts.Add(easyLayout1);
            pattern_layouts.Add(easyLayout2);
            pattern_layouts.Add(easyLayout3);
            pattern_layouts.Add(easyLayout4);
            pattern_layouts.Add(easyLayout5);
            pattern_layouts.Add(easyLayout6);
            pattern_layouts.Add(easyLayout7);
            //pattern_layouts.Add(easyLayout8);
            //pattern_layouts.Add(easyLayout9);
            //pattern_layouts.Add(easyLayout10);
        }

        public void AddMediumLayouts()
        {
            pattern_layouts.Add(mediumLayout1);
            pattern_layouts.Add(mediumLayout2);
            pattern_layouts.Add(mediumLayout3);
            pattern_layouts.Add(mediumLayout4);
            pattern_layouts.Add(mediumLayout5);
            pattern_layouts.Add(mediumLayout6);
            pattern_layouts.Add(mediumLayout7);
            pattern_layouts.Add(mediumLayout8);
            pattern_layouts.Add(mediumLayout9);
            // pattern_layouts.Add(mediumLayout10);
        }

        public void AddHardLayouts()
        {
            pattern_layouts.Add(hardLayout1);
            pattern_layouts.Add(hardLayout2);
            pattern_layouts.Add(hardLayout3);
            pattern_layouts.Add(hardLayout4);
            pattern_layouts.Add(hardLayout5);
            pattern_layouts.Add(hardLayout6);
            pattern_layouts.Add(hardLayout7);
            pattern_layouts.Add(hardLayout8);
            pattern_layouts.Add(hardLayout9);
            pattern_layouts.Add(hardLayout10);
        }

        public void AddExtremeLayouts()
        {
            pattern_layouts.Add(extremeLayout1);
            pattern_layouts.Add(extremeLayout2);
            pattern_layouts.Add(extremeLayout3);
            //  pattern_layouts.Add(extremeLayout4);
            //  pattern_layouts.Add(extremeLayout5);
            //  pattern_layouts.Add(extremeLayout6);
            //  pattern_layouts.Add(extremeLayout7);
            //  pattern_layouts.Add(extremeLayout8);
            //  pattern_layouts.Add(extremeLayout9);
            //  pattern_layouts.Add(extremeLayout10);
        }

        public List<Chunk> GiveColumn(PatternArrayLayout selected_layout, int column_number, List<Chunk> previous_column, float position_offset)
        {
            List<Chunk> column = new List<Chunk>();
            int i = selected_layout.rows.Length - 1;
            int j = 1;
            if (column.Count == 0 && previous_column == null)
                column.Add(chunk_generator.GiveChunk(null, j, column_number, position_offset, obstacle.None));
            else if (column.Count == 0 && previous_column != null)
                column.Add(chunk_generator.GiveChunk(previous_column[previous_column.Count - 1], j, column_number, position_offset, obstacle.None));
            while (i >= 0)
            {
                column.Add(chunk_generator.GiveChunk(column[j - 1], j, column_number, position_offset, selected_layout.rows[i].row[column_number - 1]));
                i--;
                j++;
            }
            return column;
        }


        PatternArrayLayout GetRandomLayout()
        {
            return pattern_layouts[Random.Range(0, pattern_layouts.Count)];
        }
    }

}