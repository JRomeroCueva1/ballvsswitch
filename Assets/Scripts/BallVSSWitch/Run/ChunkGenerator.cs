﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class ChunkGenerator : MonoBehaviour
    {
        [System.Serializable]
        public struct chunkInfo
        {
            public string code;
            public Sprite graphicSprite;
        }

        [System.Serializable]
        public struct obstacleChunkInfo
        {
            public string code;
            public Sprite graphicSprite;
            public Sprite instructionSprite;
            public Chunk.chunkAnimationStyle animationStyle;
        }

        private List<Chunk> ChunkPool;
        public RectTransform chunkPoolHolder;
        public RectTransform column1Holder;
        public RectTransform column2Holder;
        public float chunk_size = 256;
        public Color whiteChunkColor;
        public Color redChunkColor;
        public Color blueChunkColor;

        public GameObject chunkPrefab;
        public chunkInfo[] chunkInfos;
        public obstacleChunkInfo[] obstacleChunkInfos;
        public obstacleChunkInfo[] wallChunkInfos;


        public bool initialized = false;

        /* [System.Serializable]
         public enum chunkType
         {
             None,
             Regular,
             Rainbow
         };*/

        private static ChunkGenerator instance = null;

        public static ChunkGenerator Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<ChunkGenerator>();
                }
                return instance;
            }
            set { }
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            InitPool();
        }

        public RectTransform origin;
        public void InitPool()
        {
            initialized = true;
            if (chunkPoolHolder != null && (ChunkPool == null || ChunkPool.Count == 0))
            {
                ChunkPool = new List<Chunk>();
                foreach (Transform proj in chunkPoolHolder.transform)
                {
                    ChunkPool.Add(proj.GetComponent<Chunk>());
                }
            }
            SetOriginRect();
        }


        void SetOriginRect()
        {
            origin.localScale = Vector3.one;
            origin.anchoredPosition = Vector2.zero;
            origin.sizeDelta = Vector2.zero;
            origin.offsetMin = Vector2.zero;
            origin.offsetMax = Vector2.zero;
            origin.rotation = Quaternion.Euler(0, 0, 0);
            origin.pivot = new Vector2(0.5F, 0.5F);
            origin.anchorMin = Vector2.zero;
            origin.anchorMax = Vector2.one;
        }

        void ResetRect(RectTransform r)
        {
            r.localScale = origin.localScale;
            r.anchoredPosition = origin.anchoredPosition;
            r.sizeDelta = origin.sizeDelta;
            r.offsetMin = origin.offsetMin;
            r.offsetMax = origin.offsetMax;
            r.rotation = origin.rotation;
            r.pivot = origin.pivot;
            r.anchorMin = origin.anchorMin;
            r.anchorMax = origin.anchorMax;
        }

        Chunk GetPooledChunk(string chunk_code)
        {
            if (!initialized)
                InitPool();
            foreach (Chunk chunk in ChunkPool)
            {
                if (chunk != null && Vector3.Distance(chunk.transform.position, chunkPoolHolder.position) < 1)
                {
                    ActivateChunk(chunk, chunk_code, true);
                    return chunk;
                }
            }
            GameObject new_chunk = (GameObject)Instantiate(chunkPrefab, chunkPoolHolder.position, chunkPoolHolder.rotation);
            new_chunk.transform.SetParent(chunkPoolHolder.transform);
            new_chunk.transform.position = chunkPoolHolder.position;
            Chunk new_chunk_script = new_chunk.GetComponent<Chunk>();
            ActivateChunk(new_chunk_script, chunk_code, true);
            ChunkPool.Add(new_chunk_script);
            return new_chunk_script;
        }

        void ActivateChunk(Chunk chunk, string chunk_code, bool activate)
        {
            chunk.gameObject.SetActive(true);
        }

        public void ReturnPooledChunk(Chunk returnedChunk)
        {
            if (returnedChunk == null)
                return;
            ActivateChunk(returnedChunk, "", false);
            returnedChunk.transform.SetParent(chunkPoolHolder);
            returnedChunk.gameObject.transform.position = chunkPoolHolder.position;
            returnedChunk.transform.localPosition = Vector3.zero;
            returnedChunk.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }

        public Chunk GiveChunk(Chunk previous_chunk, int chunk_number, int column_number, float position_offset, PatternManager.obstacle obstacle)
        {
            string previous_code = "B852H";
            if (previous_chunk != null)
                previous_code = previous_chunk.chunkCode;
            string new_chunk_code = FindMatchingChunk(previous_code, obstacle);
            Chunk.chunkAnimationStyle force_style = Chunk.chunkAnimationStyle.None;
            if (position_offset == 0)
            {
                new_chunk_code = "B852H";
                force_style = Chunk.chunkAnimationStyle.Rotate;
            }
            Chunk selected_chunk = GetPooledChunk(new_chunk_code);

            if (column_number == 1)
                selected_chunk.transform.SetParent(column1Holder.transform);
            else
                selected_chunk.transform.SetParent(column2Holder.transform);
            if (previous_chunk == null)
                selected_chunk.rectTrans.anchoredPosition = new Vector2(0, (chunk_number * chunk_size) + (chunk_size * position_offset) - column1Holder.rect.height / 2 + chunk_size / 2);
            else
                selected_chunk.rectTrans.anchoredPosition = new Vector2(0, previous_chunk.rectTrans.anchoredPosition.y + chunk_size);
            selected_chunk.chunkCode = new_chunk_code;
            selected_chunk.rectTrans.localScale = Vector3.one;
            selected_chunk.rectTrans.sizeDelta = new Vector2(chunk_size, chunk_size);
            ResetRect(selected_chunk.graphic.rectTransform);
            if (obstacle == PatternManager.obstacle.WALL_RED || obstacle == PatternManager.obstacle.WALL_BLUE)
            {
                selected_chunk.value = Random.Range(2, 3);

            }
            else
                selected_chunk.value = 0;
            selected_chunk.SetTextPosition();
            selected_chunk.SetValueText();
            SkinChunk(selected_chunk, obstacle, force_style);
            FillBallPath(selected_chunk, column_number);

            return selected_chunk;
        }

        public List<RectTransform> column1BallPath;
        public List<RectTransform> column2BallPath;

        void FillBallPath(Chunk chunk, int column_number)
        {
            int i = 1;
            foreach (RectTransform w in chunk.waypoints)
            {
                if (chunk.chunkCode[i] == '1' || chunk.chunkCode[i] == '4' || chunk.chunkCode[i] == '7')
                    w.anchoredPosition = new Vector2(-100, w.anchoredPosition.y);
                else if (chunk.chunkCode[i] == '2' || chunk.chunkCode[i] == '5' || chunk.chunkCode[i] == '8')
                    w.anchoredPosition = new Vector2(0, w.anchoredPosition.y);
                else if (chunk.chunkCode[i] == '3' || chunk.chunkCode[i] == '6' || chunk.chunkCode[i] == '9')
                    w.anchoredPosition = new Vector2(100, w.anchoredPosition.y);
                i++;
            }


            if (column_number == 1)
                column1BallPath.AddRange(chunk.waypoints);
            else if (column_number == 2)
                column2BallPath.AddRange(chunk.waypoints);
        }

        void SkinChunk(Chunk selected_chunk, PatternManager.obstacle obstacle, Chunk.chunkAnimationStyle force_style)
        {

            if (obstacle == PatternManager.obstacle.None)
            {
                List<chunkInfo> possible_chunks = new List<chunkInfo>();
                foreach (chunkInfo info in chunkInfos)
                {
                    if (selected_chunk.chunkCode.Equals(info.code))
                        possible_chunks.Add(info);

                }
                int random_chunk = Random.Range(0, possible_chunks.Count);
                selected_chunk.graphic.sprite = possible_chunks[random_chunk].graphicSprite;
                selected_chunk.graphic.color = whiteChunkColor;
                ResetRect(selected_chunk.graphic.rectTransform);
                selected_chunk.chColor = Chunk.chunkColor.White;
                selected_chunk.instructionGraphic.enabled = false;
                selected_chunk.is_open = true;
                selected_chunk.ForcePlayOpen();
            }
            else
            {
                List<obstacleChunkInfo> possible_chunks = new List<obstacleChunkInfo>();
                if (obstacle == PatternManager.obstacle.WALL_BLUE || obstacle == PatternManager.obstacle.WALL_RED)
                {
                    foreach (obstacleChunkInfo info in wallChunkInfos)
                    {
                        if (selected_chunk.chunkCode.Equals(info.code))
                            possible_chunks.Add(info);
                    }
                    selected_chunk.PlayAnimation(Chunk.chunkAnimationStyle.Bump);
                }
                else
                {
                    foreach (obstacleChunkInfo info in obstacleChunkInfos)
                    {
                        if (selected_chunk.chunkCode.Equals(info.code))
                            possible_chunks.Add(info);
                    }
                }
                if (possible_chunks.Count == 0)
                    Debug.Log("NO MATCH " + selected_chunk.chunkCode);
                int random_chunk = Random.Range(0, possible_chunks.Count);
                if (force_style != Chunk.chunkAnimationStyle.None)
                {
                    int i = 0;
                    while (i < possible_chunks.Count)
                    {
                        if (possible_chunks[i].animationStyle == force_style)
                            break;
                        i++;
                    }
                    random_chunk = i;
                }

                selected_chunk.graphic.sprite = possible_chunks[random_chunk].graphicSprite;
                selected_chunk.animationStyle = possible_chunks[random_chunk].animationStyle;
                selected_chunk.instructionGraphic.sprite = possible_chunks[random_chunk].instructionSprite;
                selected_chunk.graphic.sprite = possible_chunks[random_chunk].graphicSprite;
                selected_chunk.instructionGraphic.enabled = true;
                selected_chunk.graphic.rectTransform.anchoredPosition = Vector2.zero;
                selected_chunk.instructionGraphic.rectTransform.anchoredPosition = Vector2.zero;
                selected_chunk.graphic.rectTransform.localScale = Vector3.one;
                selected_chunk.graphic.rectTransform.sizeDelta = Vector2.zero;
                selected_chunk.graphic.rectTransform.offsetMin = Vector2.zero;
                selected_chunk.graphic.rectTransform.offsetMax = Vector2.zero;
                string obstacle_string = obstacle.ToString();
                if (obstacle_string.Contains("RED"))
                {
                    selected_chunk.graphic.color = redChunkColor;
                    selected_chunk.chColor = Chunk.chunkColor.Red;
                }
                else if (obstacle_string.Contains("BLUE"))
                {
                    selected_chunk.graphic.color = blueChunkColor;
                    selected_chunk.chColor = Chunk.chunkColor.Blue;
                }
                else
                {
                    selected_chunk.graphic.color = whiteChunkColor;
                    selected_chunk.chColor = Chunk.chunkColor.White;
                }
                if (obstacle_string.Contains("OPEN"))
                    selected_chunk.Open();
                else
                    selected_chunk.Close();
            }
        }

        string FindMatchingChunk(string previous_code, PatternManager.obstacle obstacle)
        {
            char prev_exit = previous_code[previous_code.Length - 1];
            List<string> matches = new List<string>();
            if (obstacle == PatternManager.obstacle.None)
            {
                foreach (chunkInfo info in chunkInfos)
                {
                    char next_entrance = info.code[0];
                    if (PointMatch(next_entrance, prev_exit))
                        matches.Add(info.code);
                }
            }
            else if (obstacle == PatternManager.obstacle.WALL_RED || obstacle == PatternManager.obstacle.WALL_BLUE)
            {
                foreach (obstacleChunkInfo info in wallChunkInfos)
                {
                    char next_entrance = info.code[0];
                    if (PointMatch(next_entrance, prev_exit))
                        matches.Add(info.code);
                }
            }
            else
            {
                foreach (obstacleChunkInfo info in obstacleChunkInfos)
                {
                    char next_entrance = info.code[0];
                    if (PointMatch(next_entrance, prev_exit))
                        matches.Add(info.code);
                }
            }

            if (matches.Count == 0)
                Debug.LogError("NO MATCH");

            return matches[Random.Range(0, matches.Count)];
        }

        bool PointMatch(char entrance, char exit)
        {
            if (entrance == 'A' && exit == 'I')
                return true;
            if (entrance == 'B' && exit == 'H')
                return true;
            if (entrance == 'C' && exit == 'G')
                return true;
            if (entrance == 'D' && exit == 'L')
                return true;
            if (entrance == 'E' && exit == 'K')
                return true;
            if (entrance == 'F' && exit == 'J')
                return true;
            if (entrance == 'G' && exit == 'C')
                return true;
            if (entrance == 'H' && exit == 'B')
                return true;
            if (entrance == 'I' && exit == 'A')
                return true;
            if (entrance == 'J' && exit == 'F')
                return true;
            if (entrance == 'K' && exit == 'E')
                return true;
            if (entrance == 'L' && exit == 'D')
                return true;
            return false;
        }
    }
}