﻿using GameAnalyticsSDK;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class RunManager : MonoBehaviour
    {

        public SessionStatus session_status;
        public PatternManager patern_manager;
        public RectTransform[] columnsLeft;
        public RectTransform[] columnsRight;
        public RectTransform ballLeft;
        public RectTransform ballRight;
        public GameObject ballLeftFX;
        public GameObject ballRightFX;
        public List<Chunk> playing_chunks;
        public List<Chunk> gone_chunks;
        public float columnSpeed = 5;
        float current_column_speed;
        public difficulty current_difficulty;

        public enum SessionStatus
        {
            None,
            Running,
        }

        public enum difficulty
        {
            Easy,
            Medium,
            Hard,
            Extreme
        };

        public void Reinit()
        {
            playing_chunks = new List<Chunk>();
            gone_chunks = new List<Chunk>();
            level = 0;
            current_column_speed = 0;
            patern_manager.pattern_layouts.Clear();
            patern_manager.pattern_layouts = null;
            current_difficulty = difficulty.Easy;
            patern_manager.chunk_generator.column1BallPath = new List<RectTransform>();
            patern_manager.chunk_generator.column2BallPath = new List<RectTransform>();
            previous_pattern = null;
            ballLeft.anchoredPosition = new Vector2(-270, ballLeft.anchoredPosition.y);
            ballRight.anchoredPosition = new Vector2(270, ballRight.anchoredPosition.y);
            ballLeftFX.SetActive(false);
            ballRightFX.SetActive(false);
            ChangeSessionStatus(RunManager.SessionStatus.Running);
        }

        public void ChangeSessionStatus(SessionStatus new_session_status)
        {
            session_status = new_session_status;
        }

        private void Update()
        {
            MovePatterns();
        }


        int level = 0;
        PatternManager.Pattern previous_pattern = null;
        void MovePatterns()
        {
            if (session_status == SessionStatus.Running)
            {
                if (level == 0 || playing_chunks.Count < 20)
                {
                    previous_pattern = patern_manager.GivePattern(previous_pattern, playing_chunks.Count / 2);

                    playing_chunks.AddRange(previous_pattern.column1);
                    playing_chunks.AddRange(previous_pattern.column2);
                    if (level == 0)
                        StartCoroutine(WaitForStart(previous_pattern.column1, previous_pattern.column2));
                    level++;
                }
                MoveChunks(playing_chunks);
                MoveBalls();
                RemoveGoneChunks();
            }
        }

        IEnumerator WaitForStart(List<Chunk> column1, List<Chunk> column2)
        {
            bool open = true;
            foreach (Chunk c in column1)
            {
                if (!c.is_open)
                    open = false;
            }
            foreach (Chunk c in column2)
            {
                if (!c.is_open)
                    open = false;
            }
            yield return new WaitForSeconds(0.05F);
            if (open)
            {
                current_column_speed = columnSpeed;
                ballLeftFX.SetActive(true);
                ballRightFX.SetActive(true);
            }
            else
                StartCoroutine(WaitForStart(column1, column2));
        }

        void MoveChunks(List<Chunk> chunks)
        {
            foreach (Chunk c in chunks)
            {
                // c.rectTrans.Translate(0, -Time.deltaTime * current_column_speed, 0);
                c.rectTrans.anchoredPosition = Vector2.MoveTowards(c.rectTrans.anchoredPosition, new Vector2(0, -2000), Time.deltaTime * current_column_speed * 20);
                if (session_status != SessionStatus.None && !c.is_open && c.rectTrans.anchoredPosition.y < ballLeft.anchoredPosition.y + c.rectTrans.rect.height / 2 - ballLeft.rect.height / 2
                    && c.rectTrans.anchoredPosition.y > ballLeft.anchoredPosition.y - c.rectTrans.rect.height / 2 + ballLeft.rect.height / 2)
                {
                    ChangeSessionStatus(RunManager.SessionStatus.None);
                    GameAnalytics.NewDesignEvent("BallVSSWitch:score", ScoreManager.Instance.current_score);
                    if (ScoreManager.Instance.current_score >= ScoreManager.Instance.current_highscore)
                        GameAnalytics.NewDesignEvent("BallVSSWitch:bestscore", ScoreManager.Instance.current_score);
                    if (c.animationStyle != Chunk.chunkAnimationStyle.Bump)
                        GameAnalytics.NewDesignEvent("BallVSSWitch:deathBySwitch", 1);
                    else
                        GameAnalytics.NewDesignEvent("BallVSSWitch:deathByWall", 1);

                    StartCoroutine(EndGame(c));
                }
                if (c.rectTrans.anchoredPosition.y < -patern_manager.chunk_generator.column1Holder.rect.height / 2 - patern_manager.chunk_generator.chunk_size / 2)
                {
                    gone_chunks.Add(c);
                }
            }


        }

        IEnumerator EndGame(Chunk chunk)
        {
            if (chunk.rectTrans.position.x > 0)
            {
                FXManager.Instance.ApplyFx(ballRight.position, FXManager.fxName.BallExplode, 2);
                ballRight.gameObject.SetActive(false);

            }
            else
            {
                FXManager.Instance.ApplyFx(ballLeft.position, FXManager.fxName.BallExplode, 2);
                ballLeft.gameObject.SetActive(false);
            }
            yield return new WaitForSeconds(2F);
            ballLeft.gameObject.SetActive(true);
            ballRight.gameObject.SetActive(true);
            GameCoreManager.Instance.StartGame(false);
        }


        public void CheckToUpdateDifficulty()
        {
            if (ScoreManager.Instance.current_score > 500 && current_difficulty < difficulty.Extreme)
            {
                current_difficulty = difficulty.Extreme;
                patern_manager.AddExtremeLayouts();
            }
            else if (ScoreManager.Instance.current_score > 250 && current_difficulty < difficulty.Hard)
            {
                current_difficulty = difficulty.Hard;
                patern_manager.AddHardLayouts();
            }
            if (ScoreManager.Instance.current_score > 100 && current_difficulty < difficulty.Medium)
            {
                current_difficulty = difficulty.Medium;
                patern_manager.AddMediumLayouts();
            }
        }

        void MoveBalls()
        {
            //  Debug.Log(patern_manager.chunk_generator.column1BallPath[0].position + " " + ballLeft.position);
            if (patern_manager.chunk_generator.column1BallPath[0].position.y < ballLeft.position.y && ballLeftFX.activeSelf)
            {
                ScoreManager.Instance.AddScore(1);
                CheckToUpdateDifficulty();
                if (current_difficulty == difficulty.Easy)
                    current_column_speed += 0.15F;
                else if (current_difficulty == difficulty.Medium)
                    current_column_speed += 0.05F;
                else if (current_difficulty == difficulty.Hard)
                    current_column_speed += 0.025F;
                else if (current_difficulty == difficulty.Extreme)
                    current_column_speed += 0.010F;
                patern_manager.chunk_generator.column1BallPath.RemoveAt(0);
                patern_manager.chunk_generator.column2BallPath.RemoveAt(0);
            }
            MoveBall(patern_manager.chunk_generator.column1BallPath[0], ballLeft, -270);
            MoveBall(patern_manager.chunk_generator.column2BallPath[0], ballRight, 270);
        }

        void MoveBall(RectTransform next_waypoint, RectTransform ball, float center)
        {
            if (Math.Abs(ball.anchoredPosition.x - (next_waypoint.anchoredPosition.x + center)) > 5)
            {
                if (ball.anchoredPosition.x < next_waypoint.anchoredPosition.x + center)
                    ball.anchoredPosition = new Vector2(ball.anchoredPosition.x + (Time.deltaTime * current_column_speed * 18), ball.anchoredPosition.y);
                else if (ball.anchoredPosition.x > next_waypoint.anchoredPosition.x + center)
                    ball.anchoredPosition = new Vector2(ball.anchoredPosition.x - (Time.deltaTime * current_column_speed * 18), ball.anchoredPosition.y);
            }
            else
            {
                ball.anchoredPosition = new Vector2(next_waypoint.anchoredPosition.x + center, ball.anchoredPosition.y);

            }
        }

        public void CleanAllChunks()
        {
            foreach (Chunk c in playing_chunks)
            {
                gone_chunks.Add(c);
            }
            RemoveGoneChunks();
        }

        void RemoveGoneChunks()
        {
            while (gone_chunks != null && gone_chunks.Count > 0)
            {
                ChunkGenerator.Instance.ReturnPooledChunk(gone_chunks[0]);
                playing_chunks.Remove(gone_chunks[0]);
                gone_chunks.RemoveAt(0);
            }
        }

        public void ToggleChunks(Chunk.chunkColor color_to_toggle)
        {
            foreach (Chunk c in playing_chunks)
            {
                if (c.chColor == color_to_toggle)
                {
                    c.Toggle();
                }
            }
        }
    }
}