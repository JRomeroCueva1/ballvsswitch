﻿using UnityEngine;
using System.Collections;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class SFXGeneral : MonoBehaviour
    {

        public AudioClip[] snakeDeath;
        public AudioClip gameOver;
        public AudioClip miss;

        public enum generalSound
        {
            None,
            SnakeDeath,
            GameOver,
            Miss
        };

        public AudioClip SelectAudioClip(generalSound menu)
        {
            if (menu == generalSound.SnakeDeath)
                return snakeDeath[Random.Range(0, snakeDeath.Length)];
            if (menu == generalSound.GameOver)
                return gameOver;
            if (menu == generalSound.Miss)
                return miss;
            return null;
        }
    }
}
