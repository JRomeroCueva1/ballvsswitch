﻿using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using System.Collections.Generic;
using BallVSSWitch;

namespace BallVSSWitch
{
    public class SoundManager : MonoBehaviour
    {
        private static SoundManager instance = null;

        public static SoundManager Instance
        {
            get { return instance; }
            set { }
        }

        void Awake()
        {
            if (instance == null)
                instance = this;
        }

        SFXGeneral sfx_menus;

        [Header("AudioSetup")]
        public AudioSource musicPlayer;
        public List<AudioSource> audioPool;
        public Transform audioPoolHolder;
        public AudioSource audioPrefab;

        bool initialized;

        public void Start()
        {
            sfx_menus = GetComponent<SFXGeneral>();
            InitPool();
        }


        public void PlaySound(Transform attach_to, SFXGeneral.generalSound menus, float volume = 100, float delay = 0, float max_duration = 0)
        {
            PlayGenericSound(attach_to, volume: 100, delay: 0, max_duration: max_duration, menus: menus);
        }

        void PlayGenericSound(Transform attach_to,
                              float volume = 100,
                              float delay = 0,
                              float max_duration = 0,
                              SFXGeneral.generalSound menus = SFXGeneral.generalSound.None)
        {
            AudioSource a_source = GetPooledAudio();
            if (a_source != null)
            {
                AttachAudioSource(a_source, attach_to);
                if (menus != SFXGeneral.generalSound.None)
                    a_source.clip = sfx_menus.SelectAudioClip(menus);
                PlayClip(a_source, volume, delay, max_duration);
            }
        }

        void PlayClip(AudioSource a_source, float volume, float delay, float max_duration)
        {
            a_source.gameObject.SetActive(true);
            SetVolume(a_source, volume);
            if (delay > 0)
                a_source.PlayDelayed(delay);
            else
                a_source.Play();
            float return_delay = a_source.clip.length + delay;
            if (max_duration > 0 && return_delay > max_duration)
                return_delay = max_duration;
            StartCoroutine(ReturnToPool(a_source, return_delay));
        }

        void SetVolume(AudioSource a_source, float volume)
        {
            if (volume > 100)
            {
                volume = 100;
            }
            a_source.volume = volume / 100;
        }

        void AttachAudioSource(AudioSource a_source, Transform parent)
        {
            a_source.gameObject.transform.SetParent(parent);
            a_source.transform.localPosition = Vector3.zero;
            a_source.transform.localRotation = Quaternion.Euler(Vector3.zero);
        }

        IEnumerator ReturnToPool(AudioSource a_source, float return_delay)
        {
            yield return new WaitForSeconds(return_delay);
            if (a_source != null)
            {
                a_source.Stop();
                a_source.clip = null;
                a_source.volume = 0;
                AttachAudioSource(a_source, audioPoolHolder);
            }
        }

        public void InitPool()
        {
            initialized = true;
            if (audioPoolHolder != null && (audioPool == null || audioPool.Count == 0))
            {
                audioPool = new List<AudioSource>();
                foreach (Transform audioItem in audioPoolHolder)
                {
                    audioPool.Add(audioItem.gameObject.GetComponent<AudioSource>());
                }
            }
        }

        AudioSource GetPooledAudio()
        {
            if (!initialized)
                InitPool();
            audioPool.RemoveAll(item => item == null);
            foreach (AudioSource audioItem in audioPool)
            {
                if (Vector3.Distance(audioItem.transform.position, audioPoolHolder.position) < 1)
                {
                    return audioItem;
                }
            }
            AudioSource new_audio_item = (AudioSource)Instantiate(audioPrefab, audioPoolHolder.position, audioPoolHolder.rotation);
            new_audio_item.transform.SetParent(audioPoolHolder.transform);
            new_audio_item.transform.position = audioPoolHolder.position;
            audioPool.Add(new_audio_item);
            return new_audio_item;
        }
    }
}